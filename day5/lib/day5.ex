defmodule Day5 do
  @moduledoc """
  Polymer reactions
  """

  @doc """
      iex> Day5.react("dabAcCaCBAcCcaDA")
      "dabCBAcaDA"
  """
  def react(polymer) when is_binary(polymer),
    do: react(polymer, [])

  def react(<<letter1, rest::binary>>, [letter2 | acc]) when abs(letter1 - letter2) == 32,
    do: react(rest, acc)

  def react(<<letter, rest::binary>>, acc),
    do: react(rest, [letter | acc])

  def react(<<>>, acc),
    do: acc |> Enum.reverse() |> List.to_string()
end

# File.read!('input.txt') |> Day5.react() |> byte_size()
