defmodule Main do
  def checksum(input) do
    input
    |> String.split("\n", trim: true) # make array
    |> Enum.reduce(%{}, fn line, acc -> # create %{"3": 4, "2", 3}
      String.codepoints(line)
      |> Enum.reduce(%{}, fn char, acc2 ->
        Map.update(acc2, char, 1, &(&1 + 1))
      end)
      |> Map.values()
      |> Map.new(fn frequency -> {Integer.to_string(frequency), 1} end)
      |> Map.merge(acc, fn _k, count1, count2 -> count1 + count2 end)
    end)
    |> Map.take(["3", "2"]) # requirements want 3 and 2
    |> Map.values()
    |> Enum.reduce(1, &*/2)
  end

  def close_ids(input) do
    lines =
      String.split(input, "\n", trim: true)
      |> Enum.map(&String.to_charlist/1)

    Enum.find_value(lines, fn line ->
      Enum.find_value(lines, fn compare_line ->
        find_close_id(line, compare_line)
      end)
    end)
  end

  defp find_close_id(line, compare_line) do
    found = Enum.zip(line, compare_line)
    |> Enum.reject(fn values -> # reject different values
      Tuple.to_list(values)
      |> Enum.uniq()
      |> Enum.count() > 1
    end)
    |> Enum.map(fn values -> elem(values, 1) end)

    if Enum.count(line) - 1 == Enum.count(found) do
      found
    end
  end
end

case System.argv() do
  ["--test"] ->
    ExUnit.start()

    defmodule MainTest do
      use ExUnit.Case

      import Main

      test "checksum" do
        assert checksum("""
               aaaed
               aaaec
               zzzed
               safdf
               """) == 3
      end

      test "close_ids" do
        assert close_ids("""
               aaaed
               zzzzd
               aaaec
               """) == 'aaae'
      end
    end

  [input_file] ->
    input_file
    |> File.read!()
    |> Main.close_ids()
    |> IO.puts()

  _ ->
    IO.puts(:stderr, "aborting, need a filename or --test")
    System.halt(1)
end
