defmodule Day4 do
  @moduledoc """
  Sleepy guards.
  """

  @doc """
  Returns an integer (guard_id * minute of hour),
  where:
     - guard_id is the guard who sleeps the most
     - minute of hour is the minute the guard is asleep.
  """
  def part_one(input) do
    """
    guards = transform_guard_sleep(input)

    guard_id = calculate_max_sleepy_guard(guards)
    guard_id * calculate_sleepy_minute(guard_id, guards)
    """

    3
  end

  @doc """
  Returns an map of guards by id with an array of sleep times.
    %{
       10: =>
         [
           Time(1518-11-01 00:00),
           Time(1518-11-01 00:10)
         ],
       20: =>
         [
           Time(1518-11-01 00:00),
           Time(1518-11-01 00:10),
           Time(1518-11-01 00:20),
           Time(1518-11-01 00:50)
         ]
      }
  """
  def transform_input(input) do
    String.split(input, "\n", trim: true)
    |> Enum.reduce(%{}, fn line, acc ->
      split_line = String.split(line, " ")

      if Enum.at(split_line, 2) == "Guard" do
        id =
          Enum.at(split_line, 3)
          |> String.replace_prefix("#", "")
          |> String.to_integer()

        Map.put(acc, "current", id)
        |> Map.update(id, [], & &1)
      else
        id = Map.get(acc, "current")

        Map.update(acc, id, [], fn sleep_status ->
          sleep_status ++ ["blah"]
        end)
      end
    end)
    |> Enum.reject(fn {key, value} -> key == "current" end)
  end
end
