defmodule Day1 do
  def final_frequency(input) do
    input
    |> String.split("\n", trim: true) 
    |> Enum.map(&String.to_integer/1)
    |> Enum.sum
  end

  def repeat_frequency(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> Stream.cycle
    |> Enum.reduce_while({0, MapSet.new}, fn x, {current, seen} ->
      updated = current + x
      if MapSet.member?(seen, updated) do
        {:halt, updated}
      else
        {:cont, {updated, MapSet.put(seen, updated)}}
      end
    end)
  end
end

case System.argv() do 
  ["--test"] ->
    ExUnit.start()

    defmodule Day1Test do
      use ExUnit.Case

      import Day1

      test "final_fequency" do
        assert final_frequency("""
    +1
    +1
    +1
    """) == 3
      end
    end

  [input_file] ->
    input_file
    |> File.read!()
    |> Day1.repeat_frequency()
    |> IO.puts

  _ ->
    IO.puts :stderr, "aborting, need a filename or --test"
    System.halt(1)
end
