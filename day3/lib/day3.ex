defmodule Day3 do
  @moduledoc """
  Find total overlapping fabric, in inches.
  """

  @doc """
      iex> Day3.calculate <input.txt>
      3433
  """
  def part_one(input) do
    String.split(input, "\n", trim: true)
    |> populate_grid()
    |> overlapping_cells()
    |> Enum.count()
  end

  @doc """
     Returns an array of claim ids that do not overlap.
  """
  def part_two(input) do
    String.split(input, "\n", trim: true)
    |> populate_grid()
    |> non_overlapping_claim()
  end

  defp non_overlapping_claim(grid) do
    [ids, ids_to_remove] =
      Map.values(grid)
      |> Enum.split_with(fn cell -> Enum.count(cell) == 1 end)
      |> Tuple.to_list()
      |> Enum.map(fn list -> List.flatten(list) |> MapSet.new() end)

    MapSet.difference(ids, ids_to_remove)
    |> MapSet.to_list()
  end

  defp overlapping_cells(grid) do
    Map.values(grid)
    |> Enum.filter(fn x -> Enum.count(x) > 1 end)
  end

  # Returns the grid with coordinates as the keys and value
  # number of overlap as the value.
  defp populate_grid(lines) do
    Enum.reduce(lines, %{}, fn claim, acc ->
      [id, left, top, width, height] = split_claim(claim)

      Enum.reduce((left + 1)..(left + width), acc, fn x, acc ->
        Enum.reduce((top + 1)..(top + height), acc, fn y, acc ->
          Map.update(acc, {x, y}, [id], &[id | &1])
        end)
      end)
    end)
  end

  # Returns an array of arrays:
  # [
  #   [id, left, top, width, height]
  # ]
  #
  # input:
  # 1 @ 1,3: 4x4
  #
  # [
  #   [1, 1, 3, 4, 4]
  # ]
  defp split_claim(claim) do
    String.split(claim, ["#", " @ ", ",", ": ", "x"], trim: true)
    |> Enum.map(&String.to_integer/1)
  end
end

case System.argv() do
  ["--test"] ->
    ExUnit.start()

    defmodule MainTest do
      use ExUnit.Case

      import Day3

      test "part_one" do
        assert part_one("""
               #1 @ 1,3: 4x4
               #2 @ 3,1: 4x4
               """) == 4
      end

      test "part_two" do
        assert part_two("""
               #1 @ 1,3: 4x4
               #2 @ 3,1: 4x4
               #3 @ 5,5: 2x2
               """) == [3]
      end
    end

  ["--part_two"] ->
    "input.txt"
    |> File.read!()
    |> Day3.part_two()
    |> IO.inspect()

  ["--part_one"] ->
    "input.txt"
    |> File.read!()
    |> Day3.part_one()
    |> IO.inspect()
end
